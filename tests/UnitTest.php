<?php

namespace App\Tests;

use App\Entity\Demo;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testDemo()
    {
        // Arrange
        $demo = new Demo();
        
        // Act
        $demo->setDemo('demo');
        
        // Assert
        $this->assertEquals('demo', $demo->getDemo());
    }
}
